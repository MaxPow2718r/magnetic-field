import numpy as np

def B(x,y,C, mu_r = 1):
    Bx, By = 0, 0
    mu = 4 * np.pi * 10**(-7)
    for i in C:
        r = np.sqrt((x - i[1])**2+(y - i[-1])**2)
        mag = (mu * mu_r/(2*np.pi))*(i[0]/r)
        by = mag * (np.cos(np.arctan2(y - i[-1],x - i[1])))
        bx = mag * (-np.sin(np.arctan2(y - i[-1],x - i[1])))
        Bx = Bx + bx
        By = By + by
    return Bx,By


# C = [I, posx, posy]
def H(x,y, C):
    Bx, By = 0, 0
    for i in C:
        r = np.sqrt((x - i[1])**2+(y - i[-1])**2)
        mag = (1/(2*np.pi))*(i[0]/r)
        by = mag * (np.cos(np.arctan2(y - i[-1],x - i[1])))
        bx = mag * (-np.sin(np.arctan2(y - i[-1],x - i[1])))
        Bx = Bx + bx
        By = By + by
    return Bx,By
