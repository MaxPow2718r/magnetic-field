from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import numpy as np
import Magnetic_field
import Wire
import Fuerza

n = 10
p = 0.5
x = np.linspace(-p,p,n)
y = np.linspace(-p,p,n)

X,Y = np.meshgrid(x,y)

# Figura
fig = plt.figure()
ax = fig.gca()
C0 = [[10,0.1,0], [-10,0,0.2]]
C1 = [[10,0.1,0], [-10,0,0.2], [-10,-0.1,0], [10,0,-0.2]]
C2 = [[10,0.1,0], [-10,-0.1,0]]
C3 = [[-10,0,0.2], [10,0,-0.2]]

# Plot of the fields
Hxp,Hyp = Magnetic_field.H(X,Y,C0)                #Magnetic field

# Campo en 0,0
Hx, Hy = Magnetic_field.H(0,0,C0)

print ("Vector H", Hx, Hy)

Fx, Fy = Fuerza.F(0,0,1,C1)

print ("Fuerza", Fx, Fy)

# Plot color map

color = np.hypot(Hxp,Hyp)
plot = ax.streamplot(X,Y,Hxp,Hyp,color=color,cmap = plt.cm.inferno, density = 2,
        arrowstyle = '->', arrowsize = 1.5)

plt.title('Campo Magnético dos cables en paralelo')
fig.colorbar(plot.lines)
plt.xlabel('$x$')
plt.ylabel('$y$')
Color_cables = {True: '#FF0000', False: '#0000FF'}
for pos in C0:
    dot = pos[1], pos[-1]
    I = pos[0]
    car = plt.Circle(dot, 0.03, color = Color_cables[I>0])
    ax.add_artist(car)

plt.savefig("Cables_paralelo.pdf", bbox_inches = 'tight', pad_inches = 0)
