import numpy as np

# Para representar el cable en 3D, en 2D no sirve
def cylinder(r, posx, posy):
    phi = np.linspace(-2*np.pi,2*np.pi,100)
    x = posx + r*np.cos(phi)
    y = posy + r*np.sin(phi)
    return x,y
