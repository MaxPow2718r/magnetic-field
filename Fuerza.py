import numpy as np
import Magnetic_field

def F(x, y, I, C):
    Bx, By = Magnetic_field.B(x,y,C)
    Fx = -By * I
    Fy = Bx * I
    return Fx, Fy
