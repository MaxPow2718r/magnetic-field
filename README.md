# Calculo de campos magnético con Python

El programa permite realizar los calculos de los vectores B, H y F. Esta basado en [https://gist.github.com/mick001/5c805433046fa31aec36](https://gist.github.com/mick001/5c805433046fa31aec36). Pero se añadieron modulos extras para poder realizar todos los calculos.

Es recomendable no aumentar de gran manera el numero de puntos con los que se calcula el campo para generar los gráficos dado que va a calcular demasiado cerca del centro del cable, esto hara el radio cercano a cero y en consecuencia el campo excesivamente grande como para compararlo con el resto del gráfico.

